$(function(){

    'use strict';
    
    var black = '#000000';
    var grey = '#CCCCCC';
    var white = '#FFFFFF';

    var lav = '#993CF9';
    var blu = '#0000FF';

    var green = '#10AC27';
    var petrl = '#187575';

    var red = '#FF0000';
    var cyan = '#22DBFD';

    var olive = '#666514';
    var citron = '#FFFECF';

    var liveLgt = '#B6C440';
    var petrLgt = '#35A8A9';
    var smrgd = '#15B3A8';

    var sala = '#444689';
    var flesh = '#FE8992';

    var mud = '#ABA648';
    var carmine = '#AB423F';


    var msnry = $('.grid').masonry({
        // set itemSelector so .grid-sizer is not used in layout
        itemSelector: '.grid-item',
        // use element for option
        columnWidth: '.grid-sizer',
        percentPosition: true
    });

    var colorTrans = function(which, color, erase) {

        var erase = erase || false;

        $('.grid-item').each(function() {

            if (erase)
                $(this).show();

            if ( $(this).find('.article-header').html() == which) {
                $(this).find('.article-header, .image-overlay').css('background-color', color);
                $(this).find('.article-body, h2, h3, h4').css('color', color);
            }
            else {
                if (erase)
                    $(this).hide();
            }
        });

        msnry.masonry();
    };


    if ( $('body').hasClass('event-page') )
        var bgColMain = cyan;
    else
        var bgColMain = grey;


    var revertSecndCol = true;

    $('.nav-elem').on('mouseout', function(){
        $('body').css('background-color', bgColMain);

        if (revertSecndCol) {
            $('.grid-item').each(function() {
                $(this).find('.article-header').css('background-color', black);
                $(this).find('.image-overlay').css('background-color', 'transparent');
                $(this).find('.article-body, h2, h3, h4').css('color', black);
            })
        }
    });

    /* -- The Nav buttons "Mousevoer" -- */

    $('.home')
        .on('mouseover', function(){
            $('body').css('background-color', grey);
        });

    $('.events')
        .on('mouseover', function(){
            $('body').css('background-color', cyan);
            colorTrans('Event', cyan);
        }).on('click', function() {
            $('body').css('background-color', cyan);
            colorTrans('Event', cyan, true);
            revertSecndCol = false;
            bgColMain = cyan;
        });

    $('.projects')
        .on('mouseover', function(){
            $('body').css('background-color', flesh); // was petrl
            colorTrans('Project', flesh);
        }).on('click', function() {
            $('body').css('background-color', flesh);
            colorTrans('Project', flesh, true);
            revertSecndCol = false;
            bgColMain = flesh;
        });

    $('.news')
        .on('mouseover', function(){
            $('body').css('background-color', olive);
            colorTrans('News', olive);
        }).on('click', function() {
            $('body').css('background-color', olive);
            colorTrans('News', olive, true);
            revertSecndCol = false;
            bgColMain = olive;
        });

    $('.lab')
        .on('mouseover', function(){
            $('body').css('background-color', sala); //was flesh
            colorTrans('Lab', sala);
        }).on('click', function() {
            $('body').css('background-color', sala);
            colorTrans('Lab', sala, true);
            revertSecndCol = false;
            bgColMain = sala;
        });

    $('.course')
        .on('mouseover', function(){
            $('body').css('background-color', smrgd);
            colorTrans('Course', smrgd); // was liveLgt
        }).on('click', function() {
            $('body').css('background-color', sala);
            colorTrans('Course', smrgd, true);
            revertSecndCol = false;
            bgColMain = smrgd;
        });



    $('.about').on('mouseover', function(){
        $('body').css('background-color', blu);
        colorTrans('About', blu);
    });


});

