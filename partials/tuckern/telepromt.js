/**
 * Created by florianwalzel on 10.11.17.
 */

'use strict';

$(document).ready(function() {

    $('[data-teleprompt]').each(function() {

        // the elements text
        var txt = $(this).text();
        // an array to hold the for each character
        var aberrations = [];
        // minimum of iterations
        var minIterations = 4;
        // total number of iterations: minimum + 12 + 1 (last clearing iteration)
        var iterations = minIterations + 12 + 1;
        // dela in ms
        var delay = 1000;

        // create an array with a random number between minIterations and minIterations+10
        // for each character of elements text. This serves as an Char Code offset on each character
        for (var i=0; i<txt.length; i++) {
            aberrations.push(minIterations + Math.floor(Math.random() * 12));
        }

        (function myLoop (i, obj) {
            setTimeout(function () {
                var str = '';
                for (var m=0; m<txt.length; m++) {
                    if (aberrations[m]-1 >= 0) {
                        // an array with cursor symbols in Char Code: ░, ▒, ▓
                        var randArr = [9617, 9618 /*, 9619*/ ];
                        // push the character + its offset value to the array
                        randArr.push( txt.charCodeAt(m) + [aberrations[m]]);
                        // ad a random element from that array to the sting (it is 3:1 a cursor)
                        str += String.fromCharCode( randArr.rand() );
                        aberrations[m] -= 1;
                    }
                    else {
                        str += String.fromCharCode( txt.charCodeAt(m) );
                    }
                }
                // substitute text in Element
                obj.html(str);
                console.log(str);

                if (--i) myLoop(i, obj);
            }, delay)
        })(iterations, $(this));

    });
});
