/**
 * Created by florianwalzel on 10.11.17.
 */

'use strict';

$(document).ready(function(){

    var $win = $(window),
        $body = $('body'),
        winHeight = $win.height(),
        totalHeight = $body.height(),
        ratio, inverseRatio, sectionHeight;


    $(window).resize(function() {

        winHeight = $win.height();
        totalHeight = $body.height();
        ratio = winHeight/totalHeight*100;

        inverseRatio = totalHeight/winHeight;
        sectionHeight = winHeight * 100 / totalHeight;

        console.log('Viewport: '+ winHeight + ' Content height: ' + totalHeight + ' Ratio: ' + ratio +'%' + ' Inverse Ratio: ' + inverseRatio + ' Section Height: ' + sectionHeight);

        colorString(['b8e1fc', 'a9d2f3', '90bae4', 'cccccc', 'e9c1df'], $body);

    });

    var colorString = function( colors, obj, colorstopFactor) {

        if (! $.isArray(colors))
            return false;

        if (typeof obj === 'undefined' || obj === null)
            obj = $('body');

        var b = 'background';

        /* Old browsers */
        var fallback = '#' + colors[0];

        /* WC3 */
        var wc3 = 'linear-gradient(to bottom, ';
        if (inverseRatio > 2) {
            for (var i=0; i<inverseRatio; i++) {
                wc3 += '#' + colors[i] + ' ' + sectionHeight*i + '%,';
            }
            var short = wc3.slice(0,-1);
            wc3 = short + ')';
            console.log(wc3);
            obj.css(b,fallback).css(b,wc3);
        }


        //background: linear-gradient(to bottom, #b8e1fc 0%, #a9d2f3 10%, #90bae4 25%, #90bcea 37%, #90bff0 50%, #6ba8e5 51%, #a2daf5 83%, #bdf3fd 100%); /* W3C */



    };


    colorString(['b8e1fc', 'a9d2f3', '90bae4', 'cccccc', 'e9c1df'], $body);

});