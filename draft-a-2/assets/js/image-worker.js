(function() {

    'use strict';

    /**
     *
     * @param canvasId
     * @param dimensions
     * @param targetId
     * @constructor
     */
    var Imageworker = function(canvasId, dimensions, targetId, style) {

        this.canvasId = canvasId;
        this.dimensions = dimensions || false;
        this.targetId = targetId || false;
        this.style = style || '';

        if ($.isArray(dimensions)) {
            if (! this.targetId)
                $('body').append('<canvas id="' + canvasId + '" width="'+ this.dimensions[0] +'" height="'+ this.dimensions[1] +'" style="'+ this.style +'"></canvas>');
            else
                $('#'+targetId).append('<canvas id="' + canvasId + '" width="'+ this.dimensions[0] +'" height="'+ this.dimensions[1]+'"></canvas>');
        }

        this.canvas = $('#' + canvasId)[0];
        this.context = this.canvas.getContext('2d');
    };

    Imageworker.prototype.pixel = function(x,y) {

        var imageData = this.context.getImageData(x, y, 1, 1);

        console.log(imageData.data);

        var r = imageData.data[0] & 0xFF;
        var g = imageData.data[1] & 0xFF;
        var b = imageData.data[2] & 0xFF;

        var rgbInteger = (r << 16) + (g << 8) + (b);
        console.log(rgbInteger);
    };

    /**
     *
     * @param imageElem
     * @param dimensions
     */
    Imageworker.prototype.serialize = function (imageElem, dimensions, withCoords) {

        var imageElem = (typeof imageElem === 'string') ? $('#'+imageElem)[0] : $('#'+this.canvasId)[0] ,
            dimensions = dimensions || this.dimensions,
            withCoords = withCoords || false;

        // draw the given image element in our canvas
        this.context.drawImage(imageElem, 0, 0);

        var serializationByPattern = function(ctx, xStart, yStart, imgWidth, imgHeight) {

            var serialized = [];

            // loop over the y-axis of the given image in steps of the patern
            for (var y=yStart; y<imgHeight; y++) {
                // do the same at the x-axis
                for (var x=xStart; x<imgWidth; x++) {

                        var imageData = ctx.getImageData(x, y, 1, 1);

                        //console.log(imageData.data);

                        var r = imageData.data[0] & 0xFF;
                        var g = imageData.data[1] & 0xFF;
                        var b = imageData.data[2] & 0xFF;

                        var rgbInteger = (r << 16) + (g << 8) + (b);

                        //console.log(rgbInteger);

                        // since trainingsat must consist of values between 0 and 1 and out integer
                        // can be (R)256 * (G)256 * (B)256 = 16777216 we mus divide it by 100000000 to
                        // get a floating point number
                        if (! withCoords)
                            serialized.push(rgbInteger/100000000);
                        else
                            serialized.push([x/1000, y/1000, rgbInteger/100000000]);
                }
            }

            return serialized;
        };

        return serializationByPattern(
            this.context,
            0, 0,
            dimensions[0],
            dimensions[1]);
    };


    Imageworker.prototype.unserialize = function (arr, dimensions) {

        var dimensions = dimensions || this.dimensions;

        var unserializeByPattern = function(ctx, data, xStart, yStart, imgWidth, imgHeight) {

            for (var y=yStart; y<imgHeight; y++) {
                // do the same at the x-axis
                for (var x=xStart; x<imgWidth; x++) {

                    var intColor = data[y*imgWidth+x] * 100000000;
                    var hexColor = '#' + ('000000' + ((intColor)>>>0).toString(16)).slice(-6);

                    console.log(hexColor);

                    ctx.fillStyle = hexColor;
                    ctx.fillRect(x, y, 1, 1);

                }
            }

            return true;
        };

        return unserializeByPattern(
            this.context,
            arr,
            0, 0,
            dimensions[0],
            dimensions[1]);

    };

    Imageworker.prototype.setPixel = function(x, y, colorData) {

        var intColor = colorData * 100000000;
        var hexColor = '#' + ('000000' + ((intColor)>>>0).toString(16)).slice(-6);

        console.log(hexColor);

        this.context.fillStyle = hexColor;
        this.context.fillRect(x, y, 1, 1);

    };

    Imageworker.prototype.monocrome = function (hexColor, dimensions) {

        var dimensions = dimensions || this.dimensions,
            hexColor = hexColor || '#000000';

        this.context.fillStyle = hexColor;
        this.context.fillRect(0, 0, dimensions[0], dimensions[1]);

    };

    Imageworker.prototype.randomImage = function (dimensions) {

        var dimensions = dimensions || this.dimensions;

        for (var y=0; y<dimensions[1]; y++) {
            // do the same at the x-axis
            for (var x = 0; x < dimensions[0]; x++) {
                this.context.fillStyle = '#' + Math.floor(Math.random() * 16777215).toString(16);
                this.context.fillRect(x, y, 1, 1);
            }
        }
    };

    // Export this Imageworker to window
    window.Imageworker = Imageworker;

})();

