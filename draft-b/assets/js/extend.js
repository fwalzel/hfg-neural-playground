String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
};

String.prototype.shorten = function (n) {
    n = (typeof n === 'number') ? n*(-1) : 0;
    return this.slice(0, n);
};

String.prototype.contains = function (substr) {
    return this.indexOf(substr) >= 0;
};

Number.prototype.isOdd = function () {
    return (this % 2) == 1
};

Number.prototype.isEven = function () {
    return (this % 2) == 0
};

Array.prototype.rand = function () {
    return this[Math.floor(Math.random() * this.length)];
};